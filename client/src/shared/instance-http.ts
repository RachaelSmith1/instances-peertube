import { ResultList } from '@peertube/peertube-types'
import { GlobalStatsHistory, Instance } from '../../../shared/models'
import { buildApiUrl } from './utils'

const baseInstancePath = '/api/v1/instances'

function listInstances (options: {
  page: number
  perPage: number
  sort: string
  search?: string
  categoriesOr?: string[]
  languagesOr?: string[]
}) {
  const { page, perPage, sort, search, categoriesOr, languagesOr } = options

  const params = {
    start: ((page - 1) * perPage) + '',
    count: perPage + '',
    sort
  }

  if (search) Object.assign(params, { search })
  if (categoriesOr) Object.assign(params, { categoriesOr })
  if (languagesOr) Object.assign(params, { languagesOr })

  const url = new URL(buildApiUrl(baseInstancePath))
  url.search = new URLSearchParams(params).toString()

  return fetch(url.toString())
    .then(handleErrors)
    .then(res => res.json() as Promise<ResultList<Instance>>)
}

function addInstance (host: string) {
  return fetch(buildApiUrl(baseInstancePath), {
    method: 'POST' as const,
    headers: {
      'content-type': 'application/json',
    },
    body: JSON.stringify({ host })
  }).then(handleErrors)
}

function getInstanceStats (options: { beforeDate?: string } = {}) {
  const { beforeDate } = options

  const url = new URL(buildApiUrl(baseInstancePath) + '/stats')

  if (beforeDate) {
    url.search = new URLSearchParams({ beforeDate }).toString()
  }

  return fetch(url.toString())
    .then(handleErrors)
}

function getGlobalStatsHistory (options: { beforeDate?: string } = {}) {
  const { beforeDate } = options

  const url = new URL(buildApiUrl(baseInstancePath) + '/stats-history')

  if (beforeDate) {
    url.search = new URLSearchParams({ beforeDate }).toString()
  }

  return fetch(url.toString())
    .then(handleErrors)
    .then(res => res.json() as Promise<GlobalStatsHistory>)
}

// ---------------------------------------------------------------------------

export {
  listInstances,
  addInstance,
  getGlobalStatsHistory,
  getInstanceStats
}

// ---------------------------------------------------------------------------

function handleErrors (response: Response) {
  if (!response.ok) {
    const err = new Error(response.statusText) as any
    err.response = response

    throw err
  }

  return response
}
