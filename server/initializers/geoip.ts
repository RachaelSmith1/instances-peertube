import { downloadDbs, open, GeoIpDbName } from 'geolite2-redist'
import maxmind, { CountryResponse, Reader } from 'maxmind'
import { logger } from '../helpers/logger.js'

let _countryLookup: Promise<Reader<CountryResponse>>

function getLookup () {
  if (_countryLookup) return _countryLookup

  _countryLookup = downloadDbs()
    .then(() => {
      return open(
        GeoIpDbName.Country,
        path => maxmind.open<CountryResponse>(path)
      )
    })

  return _countryLookup
}

async function countryLookup (ip: string): Promise<CountryResponse> {
  const lookup = await getLookup()

  try {
    return lookup.get(ip)
  } catch (err) {
    logger.error({ err }, 'Cannot lookup ip.')
  }
}

export {
  countryLookup
}
