import Pino from 'pino'
import { CONFIG } from '../initializers/constants.js'

const pino = Pino({
  level: CONFIG.LOG.LEVEL,
  transport: {
    target: 'pino-pretty'
  }
})

// ---------------------------------------------------------------------------

export {
  pino as logger
}
