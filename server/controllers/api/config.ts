import express from 'express'
import { CONFIG } from '../../initializers/constants.js'
import { ServerConfig } from 'shared/models/server-config.model.js'

const configRouter = express.Router()

configRouter.get('/config',
  getConfig
)

// ---------------------------------------------------------------------------

export { configRouter }

// ---------------------------------------------------------------------------

async function getConfig (req: express.Request, res: express.Response) {
  return res.json({
    instanceClientWarning: CONFIG.INSTANCE.CLIENT_WARNING,
    reportUrl: CONFIG.INSTANCE.REPORT_URL
  } as ServerConfig)
}
