import { NSFWPolicyType } from '@peertube/peertube-types'

export type InstanceFilters = {
  start: number
  count: number
  sort: string
  signup?: string
  healthy?: string
  nsfwPolicy?: NSFWPolicyType[]
  minUserQuota?: number
  search?: string
  categoriesOr?: number[]
  languagesOr?: string[]
  liveEnabled?: string
}
