import { InstanceStats } from './instance-stats.model.js'

export interface InstanceStatsHistory {
  data: {
    date: string, // YYYY-MM-DD
    stats: InstanceStats
  }[]
}
