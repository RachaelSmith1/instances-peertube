export interface ServerConfig {
  instanceClientWarning: string
  reportUrl: string
}
